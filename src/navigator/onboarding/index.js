import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


import GetStarted from '../../screens/onboarding';
import OnBoardingScreen from '../../screens/onboarding/OnBoardingScreen';
import FoodDetails from '../../screens/onboarding/FoodDetails';

const Stack = createStackNavigator();


const OnboardingStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="GetStarted" 
        >
            <Stack.Screen
                name="GetStarted"
                component={GetStarted}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="OnBoardingScreen"
                component={OnBoardingScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="FoodDetails"
                component={FoodDetails}
                options={{
                    headerShown: false
                }}
            />
            
        </Stack.Navigator>
    );
}

export default OnboardingStack