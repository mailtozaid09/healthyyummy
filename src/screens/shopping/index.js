import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import LoginButton from '../../components/button/LoginButton'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  

  
import CheckBox from '@react-native-community/checkbox';
import { updateItemToShoppingList } from '../../redux/home/HomeActions'

const ShoppingList = ({navigation}) => {

    const dispatch = useDispatch();


    const shopping_list = useSelector(state => state.HomeReducer.shopping_list);

    console.log("shopping_list ", shopping_list);
    const updateShoppingItem = (item, value) => {

        console.log("item, valu ", item, value);

        var favItem = {
            id: item.id,
            productName: item.productName,
            productQuantity: item.productQuantity,
            isChecked: value
        }
        dispatch(updateItemToShoppingList(favItem))
    }

    const EmptyList = () => {
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', paddingVertical: 0}} >

                <Text style={styles.nodataText} >Your shopping list is empty. Start browsing the recipes and add them to your shopping list.</Text>
            </View>
        )
    }

    const ShoppingList = () => {
        return(
            <View style={{flex: 1, alignItems: 'center', paddingVertical: 0}} >
                <View>
                    {shopping_list.map((item) => (
                        <View style={{flexDirection: 'row', marginTop: 12, width: screenWidth-40, alignItems: 'center', justifyContent: 'space-between'}} >
                            <View style={{flexDirection: 'row', alignItems: 'center' }} >
                                <CheckBox
                                    disabled={false}
                                    value={item.isChecked}
                                    style={{marginRight: 10}}
                                    onValueChange={(newValue) => updateShoppingItem(item, newValue)}
                                />
                                <Text style={styles.shoppingListText} >{item.productName}</Text>
                            </View>
                            <Text style={styles.shoppingListText} >{item.productQuantity}</Text>
                        </View>
                    ))}
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.mainContainer} >
                
         
                {shopping_list.length >0
                ?
                <ShoppingList />
                :
                <EmptyList />
                }

                <View>
                <LoginButton 
                    title="Add Item to List"
                    onPress={() => {navigation.navigate('AddCartItem')}}
                />
                </View>
            </View>
           
    </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth,
        backgroundColor: colors.light_orange1,
    },
    nodataText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
        textAlign: 'center',
        paddingHorizontal: 30,
    },
    mainContainer: {
        flex: 1,
        paddingTop: 20,
        alignItems: 'center',
        //justifyContent: 'center'
    },
    shoppingListText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
        marginBottom: 10
    }
})

export default ShoppingList
