import React, { useState } from 'react';
import { Text, View, LogBox, StatusBar } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import Navigator from './src/navigator';

import { Provider } from 'react-redux';
import Store from './src/redux/store';


LogBox.ignoreAllLogs(true);


const App = () => {
    return (
        <Provider store={Store}>
          <NavigationContainer>
              <StatusBar backgroundColor = "#0D0D0D"   />  
              <Navigator />
          </NavigationContainer>
      </Provider>
    )
}

export default App
