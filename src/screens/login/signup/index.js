import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'

import LoginButton from '../../../components/button/LoginButton'
import TextButton from '../../../components/button/TextButton'
import LoginHeader from '../../../components/header/LoginHeader'
import Input from '../../../components/input'
import { colors } from '../../../global/colors'
import { fontSize } from '../../../global/fontFamily'
  
const SignUpScreen = ({navigation}) => {

    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const signUpFunc = () => {
        console.log("form => ", form);
        navigation.navigate('ProfileStack', {screen: 'CreateProfile'})
    }


    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <LoginHeader 
                    headerTitle="Sign Up"
                    title="Create an Account"
                />
                
                <View style={{paddingHorizontal: 20, flex: 1, alignItems: 'center', justifyContent: 'space-between'}} >

                    <View>
                        <Input
                            form={form.name}
                            placeholder="Zaid Ahmed"
                            label="USERNAME"
                            onChangeText={(text) => onChange({name: 'name', value: text})}
                        />
                        
                        <Input
                            form={form.email}
                            placeholder="example@abc.com"
                            label="EMAIL"
                            onChangeText={(text) => onChange({name: 'email', value: text})}
                        />

                        <Input
                            form={form.password}
                            placeholder="Min. 8 characters"
                            label="PASSWORD"
                            isPassword={true}
                            showEyeIcon={showEyeIcon}
                            onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                            onChangeText={(text) => onChange({name: 'password', value: text})}
                        />

                        <TextButton title="Forgot Password?" onPress={() => {navigation.navigate('Forgot')}} /> 
                    </View>

                    <View>
                        <LoginButton title="Sign Up" onPress={() => {signUpFunc()}} />

                        <View style={styles.alreadyTextContainer} >
                            <Text style={styles.alreadyText1} >ALREADY HAVE AN ACCOUNT? </Text>
                            <TouchableOpacity onPress={() => {navigation.navigate('SignIn')}} >
                                <Text style={styles.alreadyText2} > LOG IN</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1,
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
    },
    alreadyTextContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
    },
})

export default SignUpScreen


