import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native'
import LoginButton from '../../components/button/LoginButton';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

  
const RecipeDetails = (props) => {
    const [recipeDetails, setRecipeDetails] = useState(props?.route?.params.params);


    useEffect(() => {
        //console.log("recipeDetails => ", recipeDetails);
    }, [])
    
    
    const RecipeIngredients = () => {
        return(
            <View>
                <View style={styles.ingredientsHeader} >
                    <Text style={styles.ingredientsTitle} >Recipe Ingredients</Text>
                </View>

                <View>
                    {recipeDetails.ingredients.map((item) => (
                        <View>
                            <Text style={styles.ingredient} > <Text style={styles.bullet} >{'\u2B24'} </Text>  {item}</Text>
                        </View>
                    ))}
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <Image
                    source={{uri: recipeDetails.dishImage}}
                    style={{height: 350, width: screenWidth}}
                />

                <View style={styles.mainContainer} >
                    <Text style={styles.dishName} >{recipeDetails.dish}</Text>
                    
                    <Text style={styles.dishDescription} >{recipeDetails.description}</Text>

                    <RecipeIngredients />
                </View>
            </ScrollView>

            <LoginButton
                title="Start"
                onPress={() => {props.navigation.navigate('PreparationDetails', {params: recipeDetails})}}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1
    },
    mainContainer: {
        padding: 20,
    },
    dishName: {
        fontSize: fontSize.Heading,
        fontWeight: '600',
        color: colors.dark_blue
    },
    ingredientsHeader: {
        borderBottomWidth: 2,
        borderColor: colors.dark_blue,
        marginVertical: 10,
        paddingBottom: 8,
    },
    ingredientsTitle: {
        fontSize: fontSize.SubHeading,
        fontWeight: '600',
        color: colors.dark_blue,
    },
    bullet: {
        fontSize: 10,
        fontWeight: '600',
        color: colors.dark_blue,
    },
    ingredient: {
        marginTop: 5,
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
    },
    dishDescription: {
        marginTop: 5,
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
    }
})

export default RecipeDetails