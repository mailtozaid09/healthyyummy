import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'

import LoginButton from '../../../components/button/LoginButton'
import TextButton from '../../../components/button/TextButton'

import LoginHeader from '../../../components/header/LoginHeader'

import { colors } from '../../../global/colors'
  
const LoginScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View>
                <LoginHeader 
                    headerTitle="Healthy Yummy"
                    title=""
                    bigImage={true}
                />

                <View style={{position: 'absolute', top: 30, right: 30}} >
                    <TextButton title="Skip" onPress={() => {navigation.navigate('SignIn')}} /> 
                </View>
            </View>

            <View>
                <LoginButton title="Sign Up" isOutlined={true} onPress={() => {navigation.navigate('SignUp')}} />
                <LoginButton title="Log In"  onPress={() => {navigation.navigate('SignIn')}} />
                
            </View>

           

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.light_orange1,
    }
})

export default LoginScreen
