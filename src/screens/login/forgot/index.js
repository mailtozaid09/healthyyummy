import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'

import LoginButton from '../../../components/button/LoginButton'
import TextButton from '../../../components/button/TextButton'

import LoginHeader from '../../../components/header/LoginHeader'
import Input from '../../../components/input'

import { colors } from '../../../global/colors'
  
const ForgotPassword = ({navigation}) => {

    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };



    return (
        <SafeAreaView style={styles.container} >
             <View style={{paddingHorizontal: 20, flex: 1, alignItems: 'center', }} >
                <LoginHeader 
                    headerTitle="Forgot Password?"
                    title="Enter the email you have used in the registration"
                    bigImage={true}
                />

                <View style={{position: 'absolute', top: 30, right: 30}} >
                    <TextButton title="Back" onPress={() => {navigation.goBack()}} /> 
                </View>

                <Input
                    form={form.email}
                    placeholder="example@abc.com"
                    label="EMAIL"
                    onChangeText={(text) => onChange({name: 'email', value: text})}
                />
            </View>

            <View>
                <LoginButton title="Send Reset Password Link"  onPress={() => {navigation.navigate('SignIn')}} />
                
            </View>

           

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.light_orange1,
    }
})

export default ForgotPassword
