import { colors } from "./colors";
import { media } from "./media";

export const onboarding_data = [
    {
        id: 1,
        image: media.onboarding_1,
        title: 'Easy-to-follow Recipes',
        description: "We have prepared a list of easy-to-follow and healthy recipes next time you don't kmow what to cook",
    },
    {
        id: 2,
        image: media.onboarding_2,
        title: 'Make Your Collection',
        description: "No more forgetting what did you make, you can make your own collection of meals",
    },
    {
        id: 3,
        image: media.onboarding_3,
        title: 'Encourage Your Child',
        description: "Gain points after completing any recipe and make your child feel encourage to eat more healthy",
    },
]

export const food_details = [
    {
        "id": 1,
        "foodTitle": "Pick Your Diet",
        "data": [
            {
                id: 1,
                title: 'Classic',
            },
            {
                id: 2,
                title: 'Vegetarian',
            },
            {
                id: 3,
                title: 'Non - Vegetarian',
            },
            {
                id: 4,
                title: 'Vegan',
            },
            {
                id: 5,
                title: 'Keto',
            },
            {
                id: 6,
                title: 'Low Card',
            },
            {
                id: 7,
                title: 'Mediterranean',
            },
        ]
    },
    {
        "id": 2,
        "foodTitle": "Any Allergies?",
        "data": [
            {
                id: 1,
                title: 'Nuts',
            },
            {
                id: 2,
                title: 'Gluten',
            },
            {
                id: 3,
                title: 'Seasame',
            },
            {
                id: 4,
                title: 'Mustard',
            },
            {
                id: 5,
                title: 'Sulfite',
            },
            {
                id: 6,
                title: 'None',
            },
        ]
        
    },
    {
        "id": 3,
        "foodTitle": "Any Dislikes?",
        "data": [
            {
                id: 1,
                title: 'Onion',
            },
            {
                id: 2,
                title: 'Garlic',
            },
            {
                id: 3,
                title: 'Nuts',
            },
            {
                id: 4,
                title: 'Tuna',
            },
            {
                id: 5,
                title: 'Eggplant',
            },
            {
                id: 6,
                title: 'Bell Pepper',
            },
            {
                id: 7,
                title: 'None',
            },
        ]
    },
    {
        "id": 4,
        "foodTitle": "Servings Per Meal",
        "data": [
            {
                id: 1,
                title: '1 Serving',
            },
            {
                id: 2,
                title: '2 Serving',
            },
            {
                id: 3,
                title: '3 - 5 Serving',
            },
            {
                id: 4,
                title: 'More than 5 Serving',
            },
        ]
    },
]

export const breakfast_data = [
    {
        id: 1,
        title: 'Avocado & Egg Toast',
    },
    {
        id: 2,
        title: 'Shakshwka',
    },
    {
        id: 3,
        title: 'French Toast',
    },
    {
        id: 4,
        title: 'Oatmeal',
    },
    {
        id: 5,
        title: 'English Breakfast',
    },
    {
        id: 6,
        title: 'Raspberry Oatmeal',
    },
    {
        id: 7,
        title: 'Blueberry Yogurt',
    },
    {
        id: 7,
        title: 'Honey Crepe',
    },
]

export const profile_images = [
    {
        id: 1,
        image: media.profile1,
    },
    {
        id: 2,
        image: media.profile2,
    },
    {
        id: 3,
        image: media.profile3,
    },
    {
        id: 4,
        image: media.profile4,
    },
    {
        id: 5,
        image: media.profile5,
    },
    {
        id: 6,
        image: media.profile6,
    },
    {
        id: 7,
        image: media.profile7,
    },
    {
        id: 8,
        image: media.profile8,
    },
    {
        id: 9,
        image: media.profile9,
    },
]


export const suggestions_data = [
    {
        id: 1,
        title: 'Roasted Vegetables',
    },
    {
        id: 2,
        title: 'Paneer',
    },
    {
        id: 3,
        title: 'Chicken',
    },
    {
        id: 4,
        title: 'Biryani',
    },
    {
        id: 5,
        title: 'Salad',
    },
    {
        id: 6,
        title: 'Snack',
    },
    {
        id: 7,
        title: 'Smoothie',
    },
]



export const sampleData = [
    {
        id: 1,
    },
    {
        id: 2,
    },
]


export const food_recipes_data = 
[
    {
        "dish": "Fruit Smoothie",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "5 mins",
        "calories": "200",
        "serving": "1",
        "dishImage": "https://cookingformysoul.com/wp-content/uploads/2022/05/triple-berry-smoothie-feat-min-500x500.jpg",
        "ingredients": ["½ cup frozen strawberries", "½ banana", "¾ cup almond milk", "1 tablespoon chia seeds", "1 teaspoon honey"],
        "preparation": [
            {
                "time": "2 mins",
                "details": "Add all the ingredients to a blender"
            },
            {
                "time": "2 mins",
                "details": "Blend until smooth."
            },
            {
                "time": "1 mins",
                "details": "Pour the smoothie into a glass or jar and enjoy!"
            }
        ],
    },
    {
        "dish": "Scrambled Eggs",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "5 mins",
        "calories": "128",
        "serving": "1",
        "dishImage": "https://www.savoryexperiments.com/wp-content/uploads/2020/03/scrambled-eggs-1-500x375.jpg",
        "ingredients": ["2 eggs", "2 tablespoons milk", "1 tablespoon butter", "Salt and pepper to taste"],
        "preparation": [
            {
                "time": "1 mins",
                "details": "In a bowl, whisk together the eggs, milk, salt and pepper."
            },
            {
                "time": "2 mins",
                "details": "Melt the butter in a skillet over medium heat. Pour the egg mixture into the pan and cook, stirring occasionally, for about 3 minutes, or until the eggs are cooked through."
            },
            {
                "time": "2 mins",
                "details": "Serve the scrambled eggs with your favorite accompaniments and enjoy!"
            }
        ],
    },
    {
        "dish": "Banana Pancakes",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "15 mins",
        "calories": "175",
        "serving": "2",
        "dishImage": "https://static.onecms.io/wp-content/uploads/sites/43/2022/03/20/20334-Banana-Pancakes-mfs__2x3.jpg",
        "ingredients": ["1 banana", "1 egg", "2 tablespoons almond flour", "1 teaspoon baking powder", "Pinch of salt", "1 tablespoon butter"],
        "preparation": [
            {
                "time": "5 mins",
                "details": "In a bowl, mash the banana with a fork. Add the egg and mix until combined. Add the almond flour, baking powder and salt and mix until a thick batter forms."
            },
            {
                "time": "5 mins",
                "details": "Heat the butter in a skillet over medium heat. Pour the batter into the pan, using a ¼ cup measure for each pancake. Cook for about 2 minutes, or until golden brown. Flip and cook for 2 minutes more, or until cooked through."
            },
            {
                "time": "5 mins",
                "details": "Serve the pancakes with your favorite toppings and enjoy!"
            }
        ],
    },
    {
        "dish": "Poha",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": 10,
        "calories": 250,
        "serving": 2,
        "dishImage": "https://pipingpotcurry.com/wp-content/uploads/2020/12/Poha-Recipe-indori-Piping-Pot-Curry.jpg",
        "ingredients": ["Poha", "Onion", "Peanuts", "Curry leaves", "Mustard seeds", "Chilli powder"],
        "preparation": [
            {
                "time": "5 mins",
                "details": "In a pan, heat oil and add mustard seeds, curry leaves and peanuts. Fry them until they are golden brown."
            },
            {
                "time": "3 mins",
                "details": "Add onion and fry them until they become translucent. Add chilli powder, poha and mix well."
            },
            {
                "time": "2 mins",
                "details": "Cook the poha for 2 minutes and serve hot."
            }
        ],
    },
    {
        "dish": "Dosa",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": 15,
        "calories": 200,
        "serving": 4,
        "dishImage": "https://www.indianhealthyrecipes.com/wp-content/uploads/2021/12/dosa-recipe.jpg",
        "ingredients": ["Rice", "Urad dal", "Fenugreek seeds", "Salt", "Oil"],
        "preparation": [
            {
                "time": "5 mins",
                "details": "Soak the rice, urad dal and fenugreek seeds in water for 8 hours."
            },
            {
                "time": "5 mins",
                "details": "Grind the soaked ingredients into a fine paste. Add salt and mix well."
            },
            {
                "time": "5 mins",
                "details": "Heat a skillet and pour the batter on it. Spread it evenly and cook until both sides are golden brown. Serve hot with chutney."
            }
        ],
    },
    {
        "dish": "Yogurt Parfait",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "5 mins",
        "calories": "320",
        "serving": "1",
        "dishImage": "https://hellolittlehome.com/wp-content/uploads/2019/04/yogurt-parfait-2-500x500.jpg",
        "ingredients": ["1 cup plain Greek yogurt", "¼ cup granola", "¼ cup berries", "1 tablespoon honey"],
        "preparation": [
            {
                "time": "2 mins",
                "details": "In a bowl, combine the yogurt, granola, berries and honey and mix until combined."
            },
            {
                "time": "2 mins",
                "details": "Transfer the yogurt mixture to a glass or jar. Layer the remaining ingredients on top."
            },
            {
                "time": "1 mins",
                "details": "Serve the parfait and enjoy!"
            }
        ],
    },
    {
        "dish": "Avocado Toast",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "10 mins",
        "calories": "320",
        "serving": "1",
        "dishImage": "https://simplyfreshfoodie.com/wp-content/uploads/2021/08/DSC_0546.jpg",
        "ingredients": ["1 slice whole wheat bread", "½ avocado", "Salt and pepper to taste", "1 teaspoon olive oil"],
        "preparation": [
            {
                "time": "2 mins",
                "details": "Toast the bread in a toaster. Mash the avocado in a bowl and season with salt and pepper."
            },
            {
                "time": "5 mins",
                "details": "Spread the mashed avocado onto the toast and drizzle with olive oil. Top with your favorite toppings and enjoy!"
            },
            {
                "time": "3 mins",
                "details": ""
            }
        ],
    },
    {
        "dish": "Egg Muffins",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "20 mins",
        "calories": "120",
        "serving": "6",
        "dishImage": 'https://joyfoodsunshine.com/wp-content/uploads/2021/05/egg-muffins-recipe-egg-cups-6.jpg',
        "ingredients": ["6 eggs", "¼ cup milk", "¼ cup chopped bell pepper", "1 teaspoon olive oil", "Salt and pepper to taste"],
        "preparation": [
            {
                "time": "5 mins",
                "details": "Preheat the oven to 350°F. Grease a muffin tin with olive oil and set aside."
            },
            {
                "time": "5 mins",
                "details": "In a bowl, whisk together the eggs, milk, bell peppers, salt and pepper. Divide the mixture evenly into the muffin tin."
            },
            {
                "time": "10 mins",
                "details": "Bake for 10 minutes, or until the muffins are cooked through. Serve and enjoy!"
            }
        ],
    },
    {
        "dish": "French Toast",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "15 mins",
        "calories": "450",
        "serving": "4",
        "dishImage": "https://static.onecms.io/wp-content/uploads/sites/43/2022/03/08/7016-french-toast-mfs-010.jpg",
        "ingredients": ["4 slices whole wheat bread", "2 eggs", "2 tablespoons milk", "1 teaspoon cinnamon", "1 teaspoon vanilla extract", "2 tablespoons butter"],
        "preparation": [
            {
                "time": "5 mins",
                "details": "In a shallow bowl, whisk together the eggs, milk, cinnamon and vanilla extract. Dip each slice of bread in the egg mixture until coated."
            },
            {
                "time": "5 mins",
                "details": "Melt the butter in a skillet over medium heat. Cook the bread slices for 2 minutes per side, or until golden brown."
            },
            {
                "time": "5 mins",
                "details": "Serve the French toast with your favorite toppings and enjoy!"
            }
        ],
    },
    { 
        "dish": "Chila",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "breakfast",
        "prepTime": "20 mins",
        "calories": "150",
        "serving": "3",
        "dishImage": " https://as1.ftcdn.net/v2/jpg/05/18/57/94/1000_F_518579422_7nyUuOErLj9hANPRHHgcGteFxQHOEJdA.jpg",
        "ingredients": ["1 cup gram flour", "1 onion", "2 tomatoes", "2 green chilli", "2 tablespoons green coriander leaves", "salt to taste", "1/4 teaspoon turmeric powder", "1/2 teaspoonred chilli powder", "2 teaspoon oil"],
        "preparation": [
          {
            "time": "5 mins",
            "details": "Mix the gram flour with salt, turmeric powder, red chilli powder, 2 tablespoon oil and 2 tablespoon water to make a thick paste"
          },
          {
            "time": "10 mins",
            "details": "Chop the onion, tomatoes and green chillies. Heat 1 teaspoon oil in a non-stick pan. Add the onions, tomatoes and green chillies. Stir-fry till the onions turn golden brown. Add the gram flour paste and mix well. Add 1 tablespoon of water and mix well. Cover and cook for 5 minutes on medium flame"
          },
          {
            "time": "5 mins",
            "details": "Garnish with coriander leaves and serve hot with chutney of your choice"
          },
        ]
    },




    {
      "dish": "Biryani",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "1 hour",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://www.licious.in/blog/wp-content/uploads/2022/06/chicken-hyderabadi-biryani-01.jpg",
      "ingredients": [
        "Basmati Rice - 2 cups",
        "Vegetables - 2 cups",
        "Onions - 2",
        "Garam Masala - 1 tsp",
        "Salt - to taste",
        "Oil - 4 tbsps",
        "Ginger Garlic Paste - 1 tsp",
        "Yogurt - 1 cup"
      ],
      "preparation": [
            {
            "details": "Fry the onions in oil and add the ginger garlic paste. Add the vegetables and fry for 2 minutes.",
            "time": "10 mins"
            },
            {
            "details": "Add the yogurt, garam masala, salt, and mix well. Add the rice and mix everything together.",
            "time": "20 mins"
            },
            {
            "details": "Add 3 cups of water, cover and cook on low flame for 30 minutes.",
            "time": "30 mins"
            }
        ],
    },
    {
      "dish": "Pulav",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "45 mins",
      "calories": 350,
      "serving": "4",
      "dishImage": "https://cdn.cdnparenting.com/articles/2020/02/24151732/Paneer-Pulav-Recipe.jpg",
      "ingredients": [
        "Basmati Rice - 2 cups",
        "Vegetables - 2 cups",
        "Onions - 2",
        "Garam Masala - 1 tsp",
        "Salt - to taste",
        "Oil - 4 tbsps",
        "Ginger Garlic Paste - 1 tsp",
        "Coriander Leaves - 1/2 cup"
      ],
      "preparation": [
        {
          "details": "Fry the onions in oil and add the ginger garlic paste. Add the vegetables and fry for 2 minutes.",
          "time": "10 mins"
        },
        {
          "details": "Add the garam masala, salt, and mix well. Add the rice and mix everything together.",
          "time": "15 mins"
        },
        {
          "details": "Add 3 cups of water, cover and cook on low flame for 20 minutes. Garnish with coriander leaves.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Curd Rice",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "30 mins",
      "calories": 300,
      "serving": "4",
      "dishImage": "https://www.cookwithmanali.com/wp-content/uploads/2015/01/Curd-Rice-South-Indian.jpg",
      "ingredients": [
        "Rice - 2 cups",
        "Curd - 2 cups",
        "Salt - to taste",
        "Mustard Seeds - 1 tsp",
        "Curry Leaves - 10-12"
      ],
      "preparation": [
        {
          "details": "Cook the rice and let it cool. Mix curd into the cooled rice and add salt.",
          "time": "10 mins"
        },
        {
          "details": "Heat oil in a pan and add the mustard seeds and curry leaves. Fry for a minute.",
          "time": "10 mins"
        },
        {
          "details": "Add the tempering to the curd rice and mix well. Serve hot.",
          "time": "10 mins"
        }
        ]
    },
    {
      "dish": "Chole Bhature",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "1 hour",
      "calories": 700,
      "serving": "4",
      "dishImage": "https://i.ytimg.com/vi/csfIOfMnRGg/maxresdefault.jpg",
      "ingredients": [
        "Chole - 2 cups",
        "Bhature - 4",
        "Onions - 2",
        "Tomatoes - 2",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Soak the chole overnight and cook in a pressure cooker. Chop the onions and tomatoes.",
          "time": "30 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomatoes and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the cooked chole and mix everything together. Garnish with coriander leaves and serve with bhature.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Aloo Paratha",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "45 mins",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://www.palatesdesire.com/wp-content/uploads/2018/08/Aloo_paratha@palates_desire-scaled.jpg",
      "ingredients": [
        "Wheat Flour - 2 cups",
        "Potatoes - 4",
        "Onions - 2",
        "Ginger - 1 inch piece",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Make a dough with wheat flour, salt, oil and water. Boil the potatoes and mash them. Chop the onions and ginger.",
          "time": "15 mins"
        },
        {
          "details": "Mix the mashed potatoes, onions, ginger, garam masala, coriander leaves, and salt. Make small balls of the dough and roll them into flat breads.",
          "time": "15 mins"
        },
        {
          "details": "Put the potato mixture in the center of the rolled breads and close it. Make sure it is sealed properly. Heat a tawa and cook the parathas till golden brown.",
          "time": "15 mins"
        }
        ]
    },
    {
      "dish": "Misal Pav",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "45 mins",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://i0.wp.com/tastymunchy.com/wp-content/uploads/2021/10/20211022_083504.jpg?resize=655%2C517&ssl=1",
      "ingredients": [
        "Moth Beans - 2 cups",
        "Onions - 2",
        "Tomatoes - 2",
        "Curry Leaves - 10-12",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Soak the moth beans overnight and cook in a pressure cooker. Chop the onions and tomatoes.",
          "time": "15 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomatoes and curry leaves and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the cooked moth beans and mix everything together. Garnish with coriander leaves and serve with pav.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Vada Pav",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "45 mins",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://static.toiimg.com/thumb/81765622.cms?width=680&height=512&imgsize=1329670",
      "ingredients": [
        "Potatoes - 4",
        "Gram Flour - 2 cups",
        "Onions - 2",
        "Ginger - 1 inch piece",
        "Green Chilli - 2",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Boil the potatoes and mash them. Chop the onions and ginger. Make a batter with gram flour, salt, water.",
          "time": "15 mins"
        },
        {
          "details": "Mix the mashed potatoes, onions, ginger, green chilli, coriander leaves, and salt. Make small balls of the batter and deep fry them.",
          "time": "15 mins"
        },
        {
          "details": "Serve the vadas with pav. Garnish with coriander leaves.",
          "time": "15 mins"
        }
        ]
    },
    {
      "dish": "Rajma Chawal",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "1 hour",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://aartimadan.com/wp-content/uploads/2021/03/Rajma-Chawal.jpg",
      "ingredients": [
        "Rajma - 2 cups",
        "Rice - 2 cups",
        "Onions - 2",
        "Tomatoes - 2",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Soak the rajma overnight and cook in a pressure cooker. Cook the rice separately. Chop the onions and tomatoes.",
          "time": "30 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomatoes and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the cooked rajma and mix everything together. Garnish with coriander leaves and serve with rice.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Kadhi Chawal",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "1 hour",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://lh3.googleusercontent.com/piKkEWu1K73z6RDFxjmTCpf_zeD9tOY1_Nz0XejvCzRzHFuIZ61gLIDrrFw0XqA-x91cPl0MPISxvw_zZwb_EhqBM5VOP0yCilNm4pk=w512-rw",
      "ingredients": [
        "Curd - 2 cups",
        "Gram Flour - 2 tbsps",
        "Onions - 2",
        "Tomatoes - 2",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Whisk the curd and gram flour together. Chop the onions and tomatoes.",
          "time": "10 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomatoes and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the whisked curd and mix everything together. Simmer for 15 minutes. Garnish with coriander leaves and serve with rice.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Dal Tadka",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "1 hour",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://shwetainthekitchen.com/wp-content/uploads/2021/12/Dal-Fry-Recipe.jpg",
      "ingredients": [
        "Toor Dal - 2 cups",
        "Onions - 2",
        "Tomatoes - 2",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Soak the toor dal overnight and cook in a pressure cooker. Chop the onions and tomatoes.",
          "time": "30 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomatoes and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the cooked toor dal and mix everything together. Simmer for 15 minutes. Garnish with coriander leaves and serve with rice.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Paneer Butter Masala",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "45 mins",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://www.indianhealthyrecipes.com/wp-content/uploads/2021/07/paneer-butter-masala.jpg",
      "ingredients": [
        "Paneer - 2 cups",
        "Onions - 2",
        "Tomatoes - 2",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Chop the paneer and onions. Chop the tomatoes and make a puree. Chop the coriander leaves.",
          "time": "15 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomato puree and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the paneer and mix everything together. Simmer for 10 minutes. Garnish with coriander leaves and serve hot.",
          "time": "20 mins"
        }
        ]
    },
    {
      "dish": "Palak Paneer",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
      "dishType": "lunch",
      "prepTime": "45 mins",
      "calories": 500,
      "serving": "4",
      "dishImage": "https://spicecravings.com/wp-content/uploads/2017/08/Palak-Paneer-5-500x500.jpg",
      "ingredients": [
        "Palak - 2 cups",
        "Paneer - 2 cups",
        "Onions - 2",
        "Tomatoes - 2",
        "Ginger Garlic Paste - 1 tsp",
        "Garam Masala - 1 tsp",
        "Coriander Leaves - 1/2 cup",
        "Salt - to taste",
        "Oil - 4 tbsps"
      ],
      "preparation": [
        {
          "details": "Chop the palak and paneer. Chop the onions and tomatoes. Make a puree of the palak.",
          "time": "15 mins"
        },
        {
          "details": "Heat oil in a pan and fry the onions. Add the tomatoes and puree and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
          "time": "10 mins"
        },
        {
          "details": "Add the paneer and mix everything together. Simmer for 10 minutes. Garnish with coriander leaves and serve hot.",
          "time": "20 mins"
        }
        ]
    },




    {
        "dish": "Palak Paneer",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "dinner",
        "prepTime": "45 mins",
        "calories": 500,
        "serving": "4",
        "dishImage": "https://spicecravings.com/wp-content/uploads/2017/08/Palak-Paneer-5-500x500.jpg",
        "ingredients": [
          "Palak - 2 cups",
          "Paneer - 2 cups",
          "Onions - 2",
          "Tomatoes - 2",
          "Ginger Garlic Paste - 1 tsp",
          "Garam Masala - 1 tsp",
          "Coriander Leaves - 1/2 cup",
          "Salt - to taste",
          "Oil - 4 tbsps"
        ],
        "preparation": [
          {
            "details": "Chop the palak and paneer. Chop the onions and tomatoes. Make a puree of the palak.",
            "time": "15 mins"
          },
          {
            "details": "Heat oil in a pan and fry the onions. Add the tomatoes and puree and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
            "time": "10 mins"
          },
          {
            "details": "Add the paneer and mix everything together. Simmer for 10 minutes. Garnish with coriander leaves and serve hot.",
            "time": "20 mins"
          }
          ]
      },
      {
        "dish": "Dal Tadka",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "dinner",
        "prepTime": "1 hour",
        "calories": 500,
        "serving": "4",
        "dishImage": "https://shwetainthekitchen.com/wp-content/uploads/2021/12/Dal-Fry-Recipe.jpg",
        "ingredients": [
          "Toor Dal - 2 cups",
          "Onions - 2",
          "Tomatoes - 2",
          "Ginger Garlic Paste - 1 tsp",
          "Garam Masala - 1 tsp",
          "Coriander Leaves - 1/2 cup",
          "Salt - to taste",
          "Oil - 4 tbsps"
        ],
        "preparation": [
          {
            "details": "Soak the toor dal overnight and cook in a pressure cooker. Chop the onions and tomatoes.",
            "time": "30 mins"
          },
          {
            "details": "Heat oil in a pan and fry the onions. Add the tomatoes and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
            "time": "10 mins"
          },
          {
            "details": "Add the cooked toor dal and mix everything together. Simmer for 15 minutes. Garnish with coriander leaves and serve with rice.",
            "time": "20 mins"
          }
          ]
      },
      {
        "dish": "Paneer Butter Masala",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "dinner",
        "prepTime": "45 mins",
        "calories": 500,
        "serving": "4",
        "dishImage": "https://www.indianhealthyrecipes.com/wp-content/uploads/2021/07/paneer-butter-masala.jpg",
        "ingredients": [
          "Paneer - 2 cups",
          "Onions - 2",
          "Tomatoes - 2",
          "Ginger Garlic Paste - 1 tsp",
          "Garam Masala - 1 tsp",
          "Coriander Leaves - 1/2 cup",
          "Salt - to taste",
          "Oil - 4 tbsps"
        ],
        "preparation": [
          {
            "details": "Chop the paneer and onions. Chop the tomatoes and make a puree. Chop the coriander leaves.",
            "time": "15 mins"
          },
          {
            "details": "Heat oil in a pan and fry the onions. Add the tomato puree and fry for 2 minutes. Add the ginger garlic paste, garam masala, salt, and mix well.",
            "time": "10 mins"
          },
          {
            "details": "Add the paneer and mix everything together. Simmer for 10 minutes. Garnish with coriander leaves and serve hot.",
            "time": "20 mins"
          }
          ]
      },
      {
        "dish": "Biryani",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla ac felis a hendrerit. Phasellus nibh risus, ultricies sit amet leo quis, suscipit gravida sapien. Mauris lectus ex, finibus sed blandit ut, cursus lobortis nibh. Nam nec viverra dui, eget lacinia dui. Fusce nec ex sapien. Nunc vestibulum sem in porta condimentum. ",
        "dishType": "dinner",
        "prepTime": "1 hour",
        "calories": 500,
        "serving": "4",
        "dishImage": "https://www.licious.in/blog/wp-content/uploads/2022/06/chicken-hyderabadi-biryani-01.jpg",
        "ingredients": [
          "Basmati Rice - 2 cups",
          "Vegetables - 2 cups",
          "Onions - 2",
          "Garam Masala - 1 tsp",
          "Salt - to taste",
          "Oil - 4 tbsps",
          "Ginger Garlic Paste - 1 tsp",
          "Yogurt - 1 cup"
        ],
        "preparation": [
              {
              "details": "Fry the onions in oil and add the ginger garlic paste. Add the vegetables and fry for 2 minutes.",
              "time": "10 mins"
              },
              {
              "details": "Add the yogurt, garam masala, salt, and mix well. Add the rice and mix everything together.",
              "time": "20 mins"
              },
              {
              "details": "Add 3 cups of water, cover and cook on low flame for 30 minutes.",
              "time": "30 mins"
              }
          ],
      },



]