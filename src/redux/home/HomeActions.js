import { ADD_DISH_TO_FAV, ADD_ITEM_TO_SHOPPING_LIST, ADD_TODO_LIST, DELETE_TODO_LIST, REMOVE_DISH_FROM_FAV, UPDATE_ITEM_TO_SHOPPING_LIST, UPDATE_TODO_LIST } from "./HomeActionTypes";

let nextTodoId = 0;

export const addDishToFavorites = params => {
    return {
        type: ADD_DISH_TO_FAV,
        payload: {
            id: ++nextTodoId,
            dish: params.dish,
            description: params.description,
            dishType: params.dishType,
            prepTime: params.prepTime,
            calories: params.calories,
            serving: params.serving,
            dishImage: params.dishImage,
            ingredients: params.ingredients,
            preparation: params.preparation,
        },
    };
};


export const removeDishFromFavorites = dish => {

    return {
        type: REMOVE_DISH_FROM_FAV,
        payload: {
            dish
        },
    };
};



export const addItemToShoppingList = params => {
    return {
        type: ADD_ITEM_TO_SHOPPING_LIST,
        payload: {
            id: ++nextTodoId,
            productName: params.productName,
            productQuantity: params.productQuantity,
            isChecked: params.isChecked,
        },
    };
};


export const updateItemToShoppingList = params => {

    return {
        type: UPDATE_ITEM_TO_SHOPPING_LIST,
        payload: {
            id: params.id,
            productName: params.productName,
            productQuantity: params.productQuantity,
            isChecked: params.isChecked,
        },
    };
};



export const addTodoList = params => {
    return {
        type: ADD_TODO_LIST,
        payload: {
            id: ++nextTodoId,
            taskName: params.taskName,
            time: params.time,
            date: params.date,
            category: params.category,
            isCompleted: params.isCompleted,
            isImportant: params.isImportant,
        },
    };
};

export const deleteTodoList = id => {
    return {
        type: DELETE_TODO_LIST,
        payload: {
            id
        },
    };
};


export const updateTodoList = params => {

    return {
        type: UPDATE_TODO_LIST,
        payload: {
            id: params.id,
            taskName: params.taskName,
            time: params.time,
            date: params.date,
            category: params.category,
            isCompleted: params.isCompleted,
            isImportant: params.isImportant,
        },
    };
};
