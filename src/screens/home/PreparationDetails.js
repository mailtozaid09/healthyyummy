import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native'
import LoginButton from '../../components/button/LoginButton';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

  
const PreparationDetails = (props) => {
    
    const [recipeDetails, setRecipeDetails] = useState(props?.route?.params.params);
    const [activeId, setActiveId] = useState(0);


    useEffect(() => {
        //console.log("Prep => recipeDetails => ", recipeDetails.preparation);
    }, [])


    const changeRecipeSteps = (value) => {

        if(value=='left'){
            if(activeId > 0){
                setActiveId(prev => prev - 1)
            }else{
                console.log("baxk");
                props.navigation.goBack()
            }
        }else{
            if(activeId < 2){
                setActiveId(prev => prev + 1)
            }else{
                console.log("completed");
                props.navigation.navigate('RecipeCompleted')
            }   
        }       
    }

    const HeaderDetails = () => {
        return(
            <View style={styles.headerDetails} >
                <Text style={styles.activeId} >{activeId+1}</Text>
                <View style={styles.itemContainer} >
                    <Image source={media.stop_watch} style={{height: 18, width: 18, marginRight: 5,}} />
                    <Text style={{fontSize: 14}} >{recipeDetails.preparation[activeId].time}</Text>
                </View>

            </View>
        )
    }
    
    const DescriptionDetails = () => {
        return(
            <View style={styles.descriptionDetails} >
                <Text style={styles.descriptionDetailsText} >{recipeDetails.preparation[activeId].details}</Text>

                <View>
                    {recipeDetails.ingredients.map((item) => (
                        <View>
                            <Text style={styles.ingredient} > <Text style={styles.bullet} >{'\u2B24'} </Text>  {item}</Text>
                        </View>
                    ))}
                </View>
            </View>
        )
    }

    const BottomButtons = () => {
        return(
            <View style={styles.bottomContainer} >
                <View style={styles.bottomButtonContainer} >
                    <TouchableOpacity 
                        onPress={() => {changeRecipeSteps('left')}}
                        >
                        <Text style={styles.nextButtonText} >Back</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                        onPress={() => {changeRecipeSteps('right')}}
                        style={styles.nextButton} >
                        <Text style={styles.nextButtonText} >Next</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}} >
                        {recipeDetails?.preparation.map((item, index) => (
                            <View style={[index == activeId ? styles.activeBar : styles.inactiveBar , index == 2 ? {marginRight: 0} : {marginRight: 10}]} >
                                
                            </View>
                        ))}
                    </View>
                </View>
            </View>
        )
    }
    return (
        <SafeAreaView style={styles.container} >
                <View style={styles.mainContainer} >
                    <View>
                        <HeaderDetails />

                        <DescriptionDetails />
                    </View>

                    
                    
                    <BottomButtons />
                </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1
    },
    mainContainer: {
        paddingBottom: 20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    bottomContainer: {
        flexDirection: 'column'
    },
    bottomButtonContainer: {
        width: screenWidth-40,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
    },
    nextButtonText: {
        fontSize: fontSize.SubHeading,
        fontWeight: '600',
        color: colors.dark_blue
    },
    nextButton: {
        height: 60,
        borderRadius: 10,
        width: 140,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.dark_blue,
        borderRightWidth: 2.5,
        borderBottomWidth: 4,
        backgroundColor: colors.primary,
    },
    activeBar: {
        height: 10, 
       flex: 1,
        borderRadius: 6,
        backgroundColor: colors.primary,
    },
    inactiveBar: {
        height: 10, 
        flex: 1,
        borderRadius: 5,
        backgroundColor: colors.light_orange,
    },
    headerDetails: {
        marginTop: 15,
        width: screenWidth,
        paddingHorizontal: 20,
        marginBottom: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    activeId: {
        fontSize: 50,
        fontWeight: '600',
        color: colors.dark_blue
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        marginTop: 10,
        padding: 5,
        paddingHorizontal: 7,
        borderRadius: 4,
        backgroundColor: colors.light_orange2
    },
    descriptionDetails: {
        paddingHorizontal: 20,
    },
    descriptionDetailsText: {
        fontSize: fontSize.Heading,
        fontWeight: '600',
        color: colors.dark_blue,
        marginBottom: 20,
    },
    bullet: {
        fontSize: 10,
        fontWeight: '600',
        color: colors.dark_blue,
    },
    ingredient: {
        marginTop: 5,
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
    },
})

export default PreparationDetails