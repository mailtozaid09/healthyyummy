import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'


import TextButton from '../../components/button/TextButton'
import HeaderTitle from '../../components/header/HeaderTitle'

import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'

import { food_details, profile_images } from '../../global/sampleData'
  
const FoodDetails = ({navigation}) => {

   
    const [currentObjIdx, setCurrentObjIdx] = useState(0);
    const [detailsArr, setDetailsArr] = useState([]);
  

    const [activeId, setActiveId] = useState(1);

    const addItemToArray  = (index, obj) => {
        if(detailsArr[index]?.value){
            var newArr = [...detailsArr]
            newArr[index].value = obj
            setDetailsArr(newArr)
        }else{
            var obj = {
                id: index + 1,
                foodTitle: food_details[currentObjIdx].foodTitle,
                value: obj
            }
            setDetailsArr(prevState => [...prevState, obj])
        }
    }


    const continueButton = (value) => {

        if(value=='left'){
            if(currentObjIdx > 0){
                setCurrentObjIdx(prev => prev-1)
                setActiveId(prev => prev - 1)
            }else{
                console.log("dsad");
            }
        }else{
            if(currentObjIdx < 3){
                setActiveId(prev => prev + 1)
                setCurrentObjIdx(prev => prev+1)
            }else{
                navigation.navigate('Tabbar')
            }   
        }       
    }


    return (
        <SafeAreaView style={styles.container} >
            <View>
                <HeaderTitle
                    headerTitle={food_details[currentObjIdx].foodTitle}
                />

                <View style={{position: 'absolute', top: 30, right: 30}} >
                    <TextButton title="Skip" onPress={() => {navigation.goBack()}} /> 
                </View>
            </View>

            <View style={{flex: 1, alignItems: 'center', justifyContent: 'space-between'}} > 
                <View>
                {food_details[currentObjIdx]?.data.map((obj) => (
                    <TouchableOpacity 
                        onPress={() => addItemToArray(currentObjIdx, obj.title)}
                        style={[styles.food_item_container, {backgroundColor:  obj.title == detailsArr[currentObjIdx]?.value ?  colors.primary : colors.light_orange2}]} >
                        <Text style={styles.food_item} >{obj.title}</Text>
                    </TouchableOpacity>
                ))}
                </View>

                <View style={styles.bottomContainer} >
                    <TouchableOpacity 
                        onPress={() => continueButton('left')}
                        activeOpacity={0.5}
                        style={[styles.arrowButton,  {backgroundColor: currentObjIdx == 0 ? '#d3d3d390' : colors.primary}]} 
                        disabled={currentObjIdx == 0 ? true : false}
                        >
                        <Image source={media.left_arrow} style={styles.right_arrow} />
                    </TouchableOpacity>

                    <View style={{flexDirection: 'row'}} >
                        {food_details.map((item, index) => (
                            <View style={item.id == activeId ? styles.activeBar : styles.inactiveBar} >
                                
                            </View>
                        ))}
                    </View>

                    <TouchableOpacity 
                        onPress={() => continueButton('right')}
                        activeOpacity={0.5}
                        style={[styles.arrowButton, {backgroundColor: detailsArr[currentObjIdx]?.value ? colors.primary :  '#d3d3d390' }]}
                        disabled={detailsArr[currentObjIdx]?.value ? false : true}
                        >
                        <Image source={media.right_arrow} style={styles.right_arrow} />
                    </TouchableOpacity>

                  
                </View>
            </View>
        
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1,
    },
    food_item_container: {
        height: 50, 
        borderRadius: 10,
        width: screenWidth-40,
        paddingLeft: 20,
        marginBottom: 15,
        justifyContent: 'center',
        backgroundColor: colors.light_orange2
    },
    food_item: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue
    },
    right_arrow: {
        height: 28,
        width: 28
    },
    bottomContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between', 
        width: screenWidth,
        paddingHorizontal: 20
    },
    arrowButton: {
        height: 70, 
        width: 70,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 4,
        borderRightWidth: 2.5,
        backgroundColor: colors.primary
    },
    activeBar: {
        height: 10, 
        width: 25, 
        marginRight: 10, 
        borderRadius: 6,
        backgroundColor: colors.primary,
    },
    inactiveBar: {
        height: 10, 
        width: 10, 
        marginRight: 10, 
        borderRadius: 5,
        backgroundColor: colors.light_orange,
    },
})

export default FoodDetails


