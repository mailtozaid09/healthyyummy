import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import LoginButton from '../../components/button/LoginButton'
import Input from '../../components/input'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
import { addItemToShoppingList } from '../../redux/home/HomeActions'


const AddCartItem = ({navigation}) => {

    const dispatch = useDispatch();

    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const addNewItemFunc = () => {
        console.log("form => ", form);
        navigation.goBack()

        if(form.productName && form.productQuantity){
            var shoppingItem = {
                productName: form.productName,
                productQuantity: form.productQuantity,
                isChecked: false,
            }
            dispatch(addItemToShoppingList(shoppingItem))
        }
    }


    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.mainContainer} >
                <Input
                    form={form.productName}
                    placeholder="eg. Butter"
                    label="PRODUCT NAME"
                    onChangeText={(text) => onChange({name: 'productName', value: text})}
                />

                <Input
                    form={form.productQuantity}
                    placeholder="eg. 120 gram"
                    label="PRODUCT QUANTITY"
                    onChangeText={(text) => onChange({name: 'productQuantity', value: text})}
                />
            </View>

            <View>
                <LoginButton 
                    title="Add New Item"
                    onPress={() => {addNewItemFunc()}}
                />
            </View>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth,
        backgroundColor: colors.light_orange1,
    },
    nodataText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
        textAlign: 'center',
        paddingHorizontal: 30,
    },
    mainContainer: {
        flex: 1,
        paddingTop: 20,
        alignItems: 'center',
        //justifyContent: 'center'
    },
    shoppingListText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
    }
})

export default AddCartItem
