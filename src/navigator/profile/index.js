import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


import CreateProfile from '../../screens/profile/CreateProfile';
import UpdateProfile from '../../screens/profile/UpdateProfile';
import ProfileScreen from '../../screens/profile/ProfileScreen';
import { fontSize } from '../../global/fontFamily';
import { colors } from '../../global/colors';

const Stack = createStackNavigator();


const ProfileStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="ProfileScreen" 
        >
            <Stack.Screen
                name="ProfileScreen"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Profile',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            
            <Stack.Screen
                name="CreateProfile"
                component={CreateProfile}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="UpdateProfile"
                component={UpdateProfile}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Update Profile',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />

        </Stack.Navigator>
    );
}

export default ProfileStack