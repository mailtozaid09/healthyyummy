import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { colors } from '../../global/colors';
import { media } from '../../global/media';


import SearchScreen from '../../screens/search';
import ProfileScreen from '../../screens/profile/ProfileScreen';
import FavoritesScreen from '../../screens/favorites';


import HomeStack from '../home';
import ShoppingStack from '../shopping';
import ProfileStack from '../profile';
import { fontSize } from '../../global/fontFamily';

const Tab = createBottomTabNavigator();



export default function Tabbar() {

    LogBox.ignoreAllLogs(true)
    
    useEffect(async() => {
        //console.log("Tabbar");
    }, []);



    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarStyle: {height: 80, paddingTop: 15, borderTopWidth: 2, borderColor: colors.dark_blue, backgroundColor: colors.light_orange1},
                tabBarIcon: ({ focused, color, size }) => {
                let iconName;
    
                if (route.name === 'HomeStack') {
                    iconName = media.home
                } else if (route.name === 'Search') {
                    iconName = media.search
                } else if (route.name === 'Favorites') {
                    iconName = media.heart
                } else if (route.name === 'ShoppingStack') {
                    iconName = media.cart
                } if (route.name === 'Leader') {
                    iconName = media.ranking
                } else if (route.name === 'ProfileStack') {
                    iconName = media.user
                }  
                return(
                    <View style={[{alignItems: 'center', justifyContent: 'center', height: 50, width: 50, borderRadius: 6,  }, focused ? {backgroundColor:  colors.primary , borderRightWidth: 2.5, borderBottomWidth: 4} : null ]} >
                        <Image source={iconName} style={{height: 28, width: 28, resizeMode: 'contain'}}/>
                        {/* <Text style={{fontSize: 12, color: focused ? colors.black : colors.gray}} >{route.name}</Text> */}
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={{
                    headerShown: false,
                    headerLeft: () => null,
                    tabBarShowLabel: false
                }}
            />
            <Tab.Screen
                name="Search"
                component={SearchScreen}
                options={{
                    headerShown: true,
                    tabBarShowLabel: false,
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '600' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            <Tab.Screen
                name="Favorites"
                component={FavoritesScreen}
                options={{
                    headerShown: true,
                    tabBarShowLabel: false,
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '600' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            <Tab.Screen
                name="ShoppingStack"
                component={ShoppingStack}
                options={{
                    headerShown: false,
                    tabBarShowLabel: false
                }}
            />
            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={{
                    headerShown: false,
                    tabBarShowLabel: false
                }}
            />
        </Tab.Navigator>
    );
}
