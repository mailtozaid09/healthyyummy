export const media = {
    get_started: require('../assets/images/onboarding/get_started.png'),
    onboarding_1: require('../assets/images/onboarding/onboarding_1.png'),
    onboarding_2: require('../assets/images/onboarding/onboarding_2.png'),
    onboarding_3: require('../assets/images/onboarding/onboarding_3.png'),

    bottom_arrow: require('../assets/images/onboarding/bottom_arrow.png'),
    line_curve: require('../assets/images/onboarding/line_curve.png'),
    line_curve_big: require('../assets/images/onboarding/line_curve_big.png'),
    
    

    header_img: require('../assets/images/header_img.png'),
    carrot: require('../assets/images/carrot.png'),
    carrot_bg: require('../assets/images/carrot_bg.png'),

    left_arrow: require('../assets/images/left_arrow.png'),
    right_arrow: require('../assets/images/right_arrow.png'),


    calendar: require('../assets/images/calendar.png'),
    calories: require('../assets/images/calories.png'),
    close: require('../assets/images/close.png'),
    cart: require('../assets/images/cart.png'),

    folder: require('../assets/images/folder.png'),

    hide: require('../assets/images/hide.png'),
    home: require('../assets/images/home.png'),
    heart: require('../assets/images/heart.png'),

    heart_empty: require('../assets/images/heart_empty.png'),
    heart_filled: require('../assets/images/heart_filled.png'),



    info: require('../assets/images/info.png'),

    ranking: require('../assets/images/ranking.png'),


    search: require('../assets/images/search.png'),
    sort: require('../assets/images/sort.png'),
    stop_watch: require('../assets/images/stop_watch.png'),

    stars: require('../assets/images/stars.png'),

    tick: require('../assets/images/tick.png'),

    trophy: require('../assets/images/trophy.png'),

    user: require('../assets/images/user.png'),
    users: require('../assets/images/users.png'),


    view: require('../assets/images/view.png'),


    profile1: require('../assets/images/profile/profile1.png'),
    profile2: require('../assets/images/profile/profile2.png'),
    profile3: require('../assets/images/profile/profile3.png'),
    profile4: require('../assets/images/profile/profile4.png'),
    profile5: require('../assets/images/profile/profile5.png'),
    profile6: require('../assets/images/profile/profile6.png'),
    profile7: require('../assets/images/profile/profile7.png'),
    profile8: require('../assets/images/profile/profile8.png'),
    profile9: require('../assets/images/profile/profile9.png'),

}