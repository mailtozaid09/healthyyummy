import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'

import LoginButton from '../../components/button/LoginButton'
import TextButton from '../../components/button/TextButton'
import LoginHeader from '../../components/header/LoginHeader'

import { colors } from '../../global/colors'
import { fontSize } from '../../global/fontFamily'
import { profile_images } from '../../global/sampleData'
  
const CreateProfile = ({navigation}) => {

    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});

    const [profileId, setProfileId] = useState(0);
   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const loginFunc = () => {
        console.log("form => ", form);
        navigation.navigate('Drawer')
    }


    return (
        <SafeAreaView style={styles.container} >
            <View>
                <LoginHeader 
                    headerTitle="Create Profile"
                    title="Choose a profile picture"
                />

                <View style={{position: 'absolute', top: 30, right: 30}} >
                    <TextButton title="Back" onPress={() => {navigation.goBack()}} /> 
                </View>
                
                <View style={{paddingHorizontal: 20, flex: 1, alignItems: 'center', justifyContent: 'space-between'}} >

                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', flexWrap: 'wrap'}} >
                        {profile_images.map((item) => (
                            <TouchableOpacity onPress={() => {setProfileId(item.id)}} style={{borderWidth: item.id == profileId ? 5 : null,  height: 86, width: 86, borderRadius: 43, margin: 6, alignItems: 'center', justifyContent: 'center', borderColor: colors.primary}} >
                                <Image source={item.image} style={{height: 70, width: 70 }} />
                            </TouchableOpacity>
                        ))}
                    </View>

                    <View>
                        <LoginButton disabled={profileId != 0 ? false : true} title="Continue" onPress={() => {navigation.navigate('OnboardingStack', {screen: 'FoodDetails'})}} />
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1,
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
    },
    alreadyTextContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
    },
})

export default CreateProfile


