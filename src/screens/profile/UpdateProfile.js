import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'
import { useDispatch, useSelector } from 'react-redux'
import LoginButton from '../../components/button/LoginButton'
import Input from '../../components/input'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
import { profile_images } from '../../global/sampleData'
import { addItemToShoppingList } from '../../redux/home/HomeActions'


const UpdateProfile = ({navigation}) => {

    const dispatch = useDispatch();

    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});
    const [profileId, setProfileId] = useState(0);

   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const updateProfileFunc = () => {
        console.log("form => ", form);
        navigation.navigate('ProfileStack')
    }


    const profileDetails = {
        username: 'Zaid Ahmed',
        userImg: media.profile6,
        userPoints: 670,
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.mainContainer} >
                <View>
                    <Input
                        form={form.name}
                        placeholder="Zaid Ahmed"
                        label="USERNAME"
                        onChangeText={(text) => onChange({name: 'name', value: text})}
                    />
                    
                    <Input
                        form={form.email}
                        placeholder="example@abc.com"
                        label="EMAIL"
                        onChangeText={(text) => onChange({name: 'email', value: text})}
                    />

                    <Input
                        form={form.password}
                        placeholder="Min. 8 characters"
                        label="PASSWORD"
                        isPassword={true}
                        showEyeIcon={showEyeIcon}
                        onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                        onChangeText={(text) => onChange({name: 'password', value: text})}
                    />
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-evenly', flexWrap: 'wrap'}} >
                    {profile_images.map((item) => (
                        <TouchableOpacity onPress={() => {setProfileId(item.id)}} style={{borderWidth: item.id == profileId ? 5 : null,  height: 86, width: 86, borderRadius: 43, margin: 6, alignItems: 'center', justifyContent: 'center', borderColor: colors.primary}} >
                            <Image source={item.image} style={{height: 70, width: 70 }} />
                        </TouchableOpacity>
                    ))}
                </View>


                <View>
                    <LoginButton disabled={profileId != 0 ? false : true} title="Update Profile" onPress={() => {updateProfileFunc()}} />
                </View>

            </View>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth,
        backgroundColor: colors.light_orange1,
    },
    mainContainer: {
        flex: 1,
        paddingTop: 20,
        alignItems: 'center',
    },

})

export default UpdateProfile

