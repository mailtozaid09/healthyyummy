import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native'
import LoginButton from '../../components/button/LoginButton';

import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';

  
const RecipeCompleted = (props) => {
   
    useEffect(() => {
        //console.log("recipeDetails => ", recipeDetails);
    }, [])
  
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <Image source={media.header_img} style={styles.header_img} />

                <Image source={media.carrot_bg} style={styles.carrot_bg} />

                <Text style={styles.title}>Congratulations</Text>
                <Text style={styles.subtitle}>You made it. You have earned 50 points for making the recipe and your level has stepped up. Enjoy your meal</Text>
                
            </View>

            <LoginButton
                title="See Your Level"
                onPress={() => {
                    props.navigation.navigate('Home');
                    props.navigation.navigate('ProfileStack');
                }}
            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.light_orange1
    },
    mainContainer: {
        padding: 20,
        alignItems: 'center'
    },
    header_img: {
        height: 120,
        width: screenWidth,
        resizeMode: 'stretch'
    },
    carrot_bg: {
        marginTop: 60,
        height: 220,
        width: 220
    },  
    title: {
        fontSize: fontSize.SubHeading,
        fontWeight: '600',
        marginVertical: 15,
        color: colors.dark_blue
    },
    subtitle: {
        fontSize: fontSize.SubTitle,
        fontWeight: '600',
        marginVertical: 15,
        textAlign: 'center',
        paddingHorizontal: 20,
        color: colors.dark_blue
    },
})

export default RecipeCompleted