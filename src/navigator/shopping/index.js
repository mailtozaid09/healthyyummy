import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import AddCartItem from '../../screens/shopping/AddCartItem';
import ShoppingList from '../../screens/shopping';
import { fontSize } from '../../global/fontFamily';
import { colors } from '../../global/colors';


const Stack = createStackNavigator();


const ShoppingStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="ShoppingList" 
        >
            <Stack.Screen
                name="ShoppingList"
                component={ShoppingList}
                
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'ShoppingList',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            <Stack.Screen
                name="AddCartItem"
                component={AddCartItem}
                options={{
                    headerShown: true,
                     headerLeft: () => null,
                    headerTitle: 'Add Item',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            
        </Stack.Navigator>
    );
}

export default ShoppingStack