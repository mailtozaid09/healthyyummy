export const colors = {
    primary: '#ff8b4c',
    light_orange: '#ffb38a',
    light_orange1: '#fef1e6',
    light_orange2: '#f9d5a7',

    green: '#71d092',
    light_green1: '#adddd0',
    light_green2: '#6fc3ac',
    
    dark_blue: '#212c46',
    light_blue: '#90aacb',
    
    reddish: '#d70040',

    black: '#252a34',
    white: '#ffffff',
    gray: '#919399',
    red: '#f29999',
}