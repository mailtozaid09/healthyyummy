import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'

import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const GetStarted = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer} >
                <View style={styles.imageContainer} >
                    <Image source={media.get_started} style={styles.mainImg} />
                    <Image source={media.carrot} style={styles.carrotImg} />
                    
                    <Text style={styles.title} >Healthy Yummy</Text>
                    <Text style={styles.subtitle}>Make Eating Healthy <Text style={styles.funText} >Fun</Text> </Text>
                </View>

                <View style={styles.bottomContainer} >
               
                    <Image source={media.bottom_arrow} style={styles.bottom_arrow} />

                    <TouchableOpacity 
                        activeOpacity={0.5}
                        style={styles.arrowButton} 
                        onPress={() => {navigation.navigate('OnBoardingScreen')}}
                    >
                        <Image source={media.right_arrow} style={styles.right_arrow} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    imageContainer: {
        alignItems: 'center'
    },
    title: {
        fontSize: fontSize.Heading,
        color: colors.dark_blue,
        fontWeight: 'bold',
    },
    funText: {
        color: colors.green
    },
    subtitle: {
        fontSize: fontSize.SubHeading,
        color: colors.dark_blue,
        fontWeight: '600',
        marginTop: 40,
    },
    mainImg: {
        width: screenWidth, 
        height: 350, 
        resizeMode: 'stretch',
    },
    carrotImg: {
        height: 120,
        width: 120,
        marginTop: 30,
        resizeMode: 'contain'
    },
    bottom_arrow: {
        height: 150,
        width: screenWidth/1.5,
        resizeMode:'stretch'
    },
    right_arrow: {
        height: 28,
        width: 28
    },
    bottomContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between', 
        width: screenWidth,
        height: 65,
    },
    arrowButton: {
        height: 70, 
        width: 70,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 4,
        borderRightWidth: 2.5,
        marginRight: 20,
        backgroundColor: colors.primary
    }
})

export default GetStarted
