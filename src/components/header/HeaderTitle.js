import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const HeaderTitle = ({headerTitle, title, bigImage}) => {
    return (
        <View style={styles.container} >
            <Image source={media.header_img} style={styles.header_img} />
            
            <View style={{marginTop: -20, paddingHorizontal: 20}} >
                <Text style={styles.headerTitle} >{headerTitle}</Text>
                <Image source={media.line_curve_big} style={styles.line_curve_big} />
            </View>
{/* 
            <Text style={styles.title} >{title}</Text> */}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        marginBottom: 50,
    },
    header_img: {
        height: 120,
        width: screenWidth,
        resizeMode: 'stretch'
    },
 
    line_curve_big: {
        height: 80,
        width: 180,
        top: 5,
        position: 'absolute',
        resizeMode: 'contain',
        left: 15,
    },
    headerTitle: {
        fontSize: fontSize.Heading,
        fontWeight: '600',
        color: colors.dark_blue
    },
    title: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        textAlign: 'center',
        color: colors.dark_blue,
        marginTop: 40,
        marginBottom: 20,
    }
})

export default HeaderTitle
