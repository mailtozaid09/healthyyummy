import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'

import LoginButton from '../../../components/button/LoginButton'
import TextButton from '../../../components/button/TextButton'
import LoginHeader from '../../../components/header/LoginHeader'
import Input from '../../../components/input'
import { colors } from '../../../global/colors'
import { fontSize } from '../../../global/fontFamily'
  
const SignInScreen = ({navigation}) => {

    const [showEyeIcon, setShowEyeIcon] = useState(false);
    const [form, setForm] = useState({});
    const [errors, setErrors] = useState({});


   
    const onChange = ({ name, value }) => {
        setForm({ ...form, [name]: value });
        setErrors({})
    };


    const loginFunc = () => {
        console.log("form => ", form);
        navigation.navigate('Drawer')
    }


    return (
        <SafeAreaView style={styles.container} >
            <View>
                <LoginHeader 
                    headerTitle="Log In"
                    title="Welcome Back"
                />
                
                <View style={{paddingHorizontal: 20, flex: 1, alignItems: 'center', justifyContent: 'space-between'}} >

                    <View>
                        <Input
                            form={form.email}
                            placeholder="example@abc.com"
                            label="EMAIL"
                            onChangeText={(text) => onChange({name: 'email', value: text})}
                        />

                        <Input
                            form={form.password}
                            placeholder="Min. 8 characters"
                            label="PASSWORD"
                            isPassword={true}
                            showEyeIcon={showEyeIcon}
                            onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                            onChangeText={(text) => onChange({name: 'password', value: text})}
                        />

                        <TextButton title="Forgot Password?" onPress={() => {navigation.navigate('Forgot')}} /> 
                    </View>

                    <View>
                        <LoginButton title="Log In" onPress={() => {navigation.navigate('SignIn')}} />

                        <View style={styles.alreadyTextContainer} >
                            <Text style={styles.alreadyText1} >DON'T HAVE AN ACCOUNT? </Text>
                            <TouchableOpacity onPress={() => {navigation.navigate('SignUp')}} >
                                <Text style={styles.alreadyText2} > SIGN UP</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.light_orange1,
    },
    alreadyText1: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    alreadyText2: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
    },
    alreadyTextContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
    },
})

export default SignInScreen
