import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';


import HomeScreen from '../../screens/home';
import RecipeDetails from '../../screens/home/RecipeDetails';
import PreparationDetails from '../../screens/home/PreparationDetails';
import RecipeCompleted from '../../screens/home/RecipeCompleted';
import { fontSize } from '../../global/fontFamily';
import { colors } from '../../global/colors';


const Stack = createStackNavigator();


const HomeStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Home',
                    headerLeft: () => null,
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            <Stack.Screen
                name="RecipeDetails"
                component={RecipeDetails}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Recipe Details',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            <Stack.Screen
                name="PreparationDetails"
                component={PreparationDetails}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Preparation',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '500' },
                    headerStyle: {backgroundColor: colors.primary, }
                }}
            />
            <Stack.Screen
                name="RecipeCompleted"
                component={RecipeCompleted}
                options={{
                    headerShown: false
                }}
            />
            
            
        </Stack.Navigator>
    );
}

export default HomeStack