import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'
import LoginButton from '../../components/button/LoginButton'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const FavoritesScreen = ({navigation}) => {

    const fav_list = useSelector(state => state.HomeReducer.fav_list);

    const FavList = () => {
        return(
            <ScrollView >
            <View style={{flex: 1, width: screenWidth, padding: 20}} >
            {fav_list.map((item) => (
                <TouchableOpacity 
                    onPress={() => {
                        navigation.navigate('RecipeDetails', {params: item});
                    }}
                    activeOpacity={0.5}
                    style={styles.searchedItem} >
                    <Image source={{uri: item.dishImage}} style={{height: 100, width: 100, borderRadius: 10}} />
                    <View style={styles.searchedItemContent} >
                        <Text style={styles.searchedItemText} >{item.dish}</Text>
                        <View style={{flexDirection: 'row', flexWrap: 'wrap'}} >
                            <View style={styles.itemContainer} >
                                <Image source={media.stop_watch} style={{height: 18, width: 18, marginRight: 5,}} />
                                <Text style={{fontSize: 14}} >{item.prepTime}</Text>
                            </View>

                            <View style={styles.itemContainer} >
                                <Image source={media.calories} style={{height: 18, width: 18, marginRight: 5,}} />
                                <Text style={{fontSize: 14}}>{item.calories} Cal</Text>
                            </View>

                            <View style={styles.itemContainer} >
                                <Image source={media.users} style={{height: 18, width: 18, marginRight: 5,}} />
                                <Text style={{fontSize: 14}}>{item.serving} People</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            ))}
            </View>
        </ScrollView>
        )
    }

    const EmptyList = () => {
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'space-between', paddingVertical: 0}} >
                <Text> </Text>
                <Text style={styles.nodataText} >Your favorite list is empty. Start browsing the recipes and add them to your favorites list.</Text>
                <LoginButton 
                    title="Browse Recipes"
                    onPress={() => {navigation.navigate('Home')}}
                />
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            

            <View>
                {fav_list.length > 0
                ?
                <FavList />
                :
                <EmptyList />
                }
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth,
        backgroundColor: colors.light_orange1,
    },
    nodataText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
        textAlign: 'center',
        paddingHorizontal: 30,
    },

    searchedItemText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    searchedItem: {
        backgroundColor: colors.white,
        borderRadius: 10,
        marginBottom: 15,
        padding: 10,
        flexDirection: 'row'
    },
    searchedItemContent: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'center'
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        marginTop: 10,
        padding: 5,
        paddingHorizontal: 7,
        borderRadius: 4,
        backgroundColor: colors.light_orange2
    },
})

export default FavoritesScreen