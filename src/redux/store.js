import {createStore, combineReducers} from 'redux';
import HomeReducer from './home/HomeReducer';


const rootReducer = combineReducers({
    HomeReducer: HomeReducer,
});

const Store = createStore(
    rootReducer
);

export default Store;
