import { food_recipes_data } from '../../global/sampleData';
import { ADD_DISH_TO_FAV, ADD_ITEM_TO_SHOPPING_LIST, ADD_TODO_LIST, DELETE_TODO_LIST, REMOVE_DISH_FROM_FAV, UPDATE_ITEM_TO_SHOPPING_LIST, UPDATE_TODO_LIST } from './HomeActionTypes';


const initialState = {
    todo_list: [],
    fav_list: [],
    shopping_list: [],
    recipes_list: food_recipes_data
};


export default HomeReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH_TO_FAV: {
            const {
                dish,
                description,
                dishType,
                prepTime,
                calories,
                serving,
                dishImage,
                ingredients,
                preparation,
            } = action.payload
            return {
                ...state,
                fav_list: [
                    ...state.fav_list, {
                        dish,
                        description,
                        dishType,
                        prepTime,
                        calories,
                        serving,
                        dishImage,
                        ingredients,
                        preparation,
                    }
                ]
            };
        }

        case REMOVE_DISH_FROM_FAV: {
            const { dish } = action.payload
                return {
                    ...state,
                    fav_list: state.fav_list.filter((item) => item.dish != dish)
            };
        }


        
        case ADD_ITEM_TO_SHOPPING_LIST: {
            const {
                id, 
                productName,
                productQuantity,
                isChecked,
               
            } = action.payload
            return {
                ...state,
                shopping_list: [
                    ...state.shopping_list, {
                        id, 
                        productName,
                        productQuantity,
                        isChecked,
                    }
                ]
            };
        }

        case UPDATE_ITEM_TO_SHOPPING_LIST:{
            const { 
                productName,
                productQuantity,
                isChecked, 
            } = action.payload
            const updatedTodo = state.shopping_list.map(todo => {
                if (todo.id != action.payload.id) {
                  return todo;
                } else {
                  return {
                    ...todo,
                        productName: productName,
                        productQuantity: productQuantity,
                        isChecked: isChecked,
                  };
                }
              });
           
            return {
                ...state,
                shopping_list: updatedTodo
            };}
            
            
            
        case ADD_TODO_LIST: {
            const {
                id, 
                title,
                isCompleted,
            } = action.payload
            return {
                ...state,
                todo_list: [
                    ...state.todo_list, {
                        id, 
                        title,
                        isCompleted,
                    }
                ]
            };
        }

        case UPDATE_TODO_LIST:{
            const { title,isCompleted, } = action.payload
            const updatedTodo = state.todo_list.map(todo => {
                if (todo.id != action.payload.id) {
                  return todo;
                } else {
                  return {
                    ...todo,
                    title: title,
                    isCompleted: isCompleted,
                  };
                }
              });
           
            return {
                ...state,
                todo_list: updatedTodo
            };}

        case DELETE_TODO_LIST: {
            const { id } = action.payload
                return {
                    ...state,
                    todo_list: state.todo_list.filter((todo) => todo.id != id)
            };
        }



        default:
            return state;
    }
};
