import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const TextButton = ({ title, onPress, isOutlined, skipButton }) => {
    return (
        <>
            <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onPress}
                    >
                    <Text style={styles.skipText} >{title}</Text>
                </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        height: 60,
        borderRadius: 10,
        backgroundColor: colors.primary,
        borderRightWidth: 2.5,
        borderBottomWidth: 4,
        marginVertical: 10,
    },
    outlinedContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
        height: 60,
        borderRadius: 10,
        borderColor: colors.dark_blue,
        borderWidth: 2.5,
    },
    title: {
        fontSize: fontSize.Title,
        fontWeight: 'bold',
        textAlign: 'center',
        color: colors.dark_blue,
    },
    skipText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        textAlign: 'center',
        color: colors.dark_blue,
        textDecorationLine: 'underline',
        marginVertical: 10,
        marginRight: 10,
    },
})

export default TextButton
