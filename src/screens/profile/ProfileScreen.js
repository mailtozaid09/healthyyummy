import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'
import { useDispatch, useSelector } from 'react-redux'
import LoginButton from '../../components/button/LoginButton'
import Input from '../../components/input'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
import { addItemToShoppingList } from '../../redux/home/HomeActions'


const ProfileScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const profileDetails = {
        username: 'Zaid Ahmed',
        userImg: media.profile6,
        userPoints: 670,
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.mainContainer} >
                <View style={{height: 200, width: 200, borderRadius: 100, backgroundColor: colors.light_orange, alignItems: 'center', justifyContent: 'center', marginBottom: 20}} >
                    <Image source={profileDetails.userImg} style={{height: 140, width: 140}} />
                </View>
                
                <Text style={{fontSize: fontSize.Heading, fontWeight: '600', color: colors.dark_blue}} >{profileDetails.username}</Text>

                <View style={{alignItems: 'center', flexDirection: 'row', marginTop: 10, marginBottom: 30}} >
                    <Image source={media.stars} style={{height: 28, width: 28, marginRight: 15}} />
                    <Text style={{fontSize: fontSize.SubTitle, fontWeight: 'bold', color: colors.light_orange}} >{profileDetails.userPoints} Points</Text>
                </View>



                <View>
                    <TouchableOpacity 
                        onPress={() => {}}
                        style={styles.cardContainer} >
                        <Image source={media.calendar} style={{height: 28, width: 28, marginRight: 15}} />
                        <Text style={styles.cardText} >Week Meal Planning</Text>
                    </TouchableOpacity>


                    <TouchableOpacity 
                        onPress={() => {}}
                        style={styles.cardContainer} >
                        <Image source={media.trophy} style={{height: 28, width: 28, marginRight: 15}} />
                        <Text style={styles.cardText} >Order Between Competitors</Text>
                    </TouchableOpacity>


                    <TouchableOpacity 
                        onPress={() => {}}
                        style={styles.cardContainer} >
                        <Image source={media.folder} style={{height: 28, width: 28, marginRight: 15}} />
                        <Text style={styles.cardText} >Previously Made Recipes</Text>
                    </TouchableOpacity>


                    <TouchableOpacity 
                        onPress={() => {navigation.navigate('UpdateProfile')}}
                        style={styles.cardContainer} >
                        <Image source={media.user} style={{height: 28, width: 28, marginRight: 15}} />
                        <Text style={styles.cardText} >Profile Updates</Text>
                    </TouchableOpacity>

                </View>
            </View>
           
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth,
        backgroundColor: colors.light_orange1,
    },
    mainContainer: {
        flex: 1,
        paddingTop: 20,
        alignItems: 'center',
        //justifyContent: 'center'
    },
    cardContainer: {
        alignItems: 'center',
        width: screenWidth-40, 
        backgroundColor: colors.light_green1, 
        paddingHorizontal: 20, 
        padding: 12, 
        borderRadius: 10, 
        flexDirection: 'row', 
        marginTop: 10, 
    },
    cardText: {
        fontSize: fontSize.SubTitle, 
        fontWeight: '500', 
        color: colors.dark_blue,
    }

})

export default ProfileScreen
