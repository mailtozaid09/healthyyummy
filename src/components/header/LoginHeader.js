import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
  
const LoginHeader = ({headerTitle, title, bigImage}) => {
    return (
        <View style={styles.container} >
            <Image source={media.header_img} style={styles.header_img} />
            {bigImage 
                ?
                <Image source={media.carrot} style={styles.big_carrot_img} />
                :
                <Image source={media.carrot} style={styles.carrot_img} />
            }

            <View style={{alignItems: 'center'}} >
                <Text style={styles.headerTitle} >{headerTitle}</Text>
                <Image source={bigImage ? media.line_curve_big : media.line_curve} style={bigImage ? styles.line_curve_big : styles.line_curve} />
            </View>

            <Text style={styles.title} >{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    header_img: {
        height: 120,
        width: screenWidth,
        resizeMode: 'stretch'
    },
    carrot_img: {
        height: 120,
        width: 120,
        resizeMode: 'contain'
    },
    big_carrot_img: {
        height: 220,
        width: 220,
        resizeMode: 'contain'
    },
    line_curve: {
        height: 100,
        width: 100,
        position: 'absolute',
        resizeMode: 'contain',
    },
    line_curve_big: {
        height: 100,
        width: 240,
        position: 'absolute',
        resizeMode: 'contain',
    },
    headerTitle: {
        fontSize: fontSize.Heading,
        fontWeight: '600',
        color: colors.dark_blue
    },
    title: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        textAlign: 'center',
        color: colors.dark_blue,
        marginTop: 40,
        marginBottom: 20,
    }
})

export default LoginHeader
