import React, { useState } from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'

import { useSelector } from 'react-redux';
import Input from '../../components/input'
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';
import { suggestions_data } from '../../global/sampleData';
  
const SearchScreen = ({navigation}) => {

    const recipes_list = useSelector(state => state.HomeReducer.recipes_list);

    const [searchValue, setSearchValue] = useState('');

    const [originalArray, setOriginalArray] = useState(recipes_list);
    const [recipeArray, setRecipeArray] = useState(recipes_list);


    const searchFilterFunction = (text) => {
        var recipesListData = recipes_list
        if (text) {
            const newData = recipesListData.filter(
                function (item) {
                const itemData = item.dish
                    ? item.dish.toUpperCase()
                    : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
        });

          
            setRecipeArray(newData);
            setSearchValue(text);
        } else {
            setRecipeArray(recipesListData);
            setSearchValue(text);
        }
    };


    const Suggestions = () => {
        return(
            <View>
                <Text style={styles.suggestionsTitle} >Suggestions</Text>
                <View style={styles.suggestionsContainer} >
                    {suggestions_data.map((item) => (
                        <TouchableOpacity 
                            onPress={() => {searchFilterFunction(item.title);}}
                            style={styles.suggestionsItem} >
                            <Text style={styles.suggestionsItemText} >{item.title}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        )
    }

    const SearchResult = () => {
        return(
            <View>
                
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10}} >
                    <Text style={styles.suggestionsTitle} >Search Result</Text>
                    <TouchableOpacity 
                        onPress={() => {}}
                        activeOpacity={0.5}
                        style={styles.sortContainer} >
                        <Text style={styles.suggestionsTitle} >Sort</Text>
                        <Image source={media.sort} style={{height: 22, width: 22, marginLeft: 5}} />
                    </TouchableOpacity>
                </View>
                <View >
                    {recipeArray.map((item) => (
                        <TouchableOpacity 
                            onPress={() => {
                                navigation.navigate('RecipeDetails', {params: item});
                                setSearchValue(''); 
                                setRecipeArray(originalArray);
                            }}
                            activeOpacity={0.5}
                            style={styles.searchedItem} >
                            <Image source={{uri: item.dishImage}} style={{height: 100, width: 100, borderRadius: 10}} />
                            <View style={styles.searchedItemContent} >
                                <Text style={styles.searchedItemText} >{item.dish}</Text>
                                <View style={{flexDirection: 'row', flexWrap: 'wrap'}} >
                                    <View style={styles.itemContainer} >
                                        <Image source={media.stop_watch} style={{height: 18, width: 18, marginRight: 5,}} />
                                        <Text style={{fontSize: 14}} >{item.prepTime}</Text>
                                    </View>

                                    <View style={styles.itemContainer} >
                                        <Image source={media.calories} style={{height: 18, width: 18, marginRight: 5,}} />
                                        <Text style={{fontSize: 14}}>{item.calories} Cal</Text>
                                    </View>

                                    <View style={styles.itemContainer} >
                                        <Image source={media.users} style={{height: 18, width: 18, marginRight: 5,}} />
                                        <Text style={{fontSize: 14}}>{item.serving} People</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView contentContainerStyle={{width: screenWidth, padding: 20}} >
                <View style={{marginBottom: 20}} >
                    <Input
                        isSearch={true}
                        placeholder="Search"
                        value={searchValue}
                        onClearSearchText={() => {setSearchValue(''); setRecipeArray(originalArray);}}
                        onChangeText={(text) =>  {searchFilterFunction(text)}}
                    />
                </View>

                {!searchValue ? 
                <Suggestions />
                    :
                <SearchResult />
                }

            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: screenWidth,
        backgroundColor: colors.light_orange1,
    },
    suggestionsContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        width: screenWidth-40,
    },
    suggestionsTitle: {
        fontSize: fontSize.SubHeading,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    suggestionsItem: {
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        margin: 5,
        borderRadius: 4,
        backgroundColor: colors.light_orange2
    },
    suggestionsItemText: {
        fontSize: fontSize.Body,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    searchedItemText: {
        fontSize: fontSize.Title,
        fontWeight: '500',
        color: colors.dark_blue,
    },
    searchedItem: {
        backgroundColor: colors.white,
        borderRadius: 10,
        marginBottom: 15,
        padding: 10,
        flexDirection: 'row'
    },
    searchedItemContent: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'center'
    },
    itemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        marginTop: 10,
        padding: 5,
        paddingHorizontal: 7,
        borderRadius: 4,
        backgroundColor: colors.light_orange2
    },
    sortContainer: {
        flexDirection: 'row', 
        alignItems: 'center',
        backgroundColor: colors.primary,
        padding: 6,
        paddingHorizontal: 10,
        borderRadius: 6,
    }
})

export default SearchScreen