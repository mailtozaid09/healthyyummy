import React, {useState, useEffect} from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, FlatList, TouchableOpacity, LogBox } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../global/colors';
import { screenWidth } from '../../global/constants';
import { fontSize } from '../../global/fontFamily';
import { media } from '../../global/media';
import { food_recipes_data } from '../../global/sampleData';
import { addDishToFavorites, removeDishFromFavorites } from '../../redux/home/HomeActions';
  
const HomeScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const recipes_list = useSelector(state => state.HomeReducer.recipes_list);

    const fav_list = useSelector(state => state.HomeReducer.fav_list);

    useEffect(() => {
        groupTodoByCategory()

        //console.log("recipes_list => ", recipes_list);

      

    }, [])



    const dishIncludedInFav = (dish) => {
        return fav_list?.some(value => value['dish'] == dish);
    }
    

    const groupTodoByCategory = (recipes) => {
       
        const groups = recipes?.reduce((groups, item) => ({
            ...groups,
            [item.dishType]: [...(groups[item.dishType] || []), item]
        }), {});

        return groups
    }

    const addDishToFav = (item) => {

        if(dishIncludedInFav(item.dish)){
            dispatch(removeDishFromFavorites(item.dish))
        }else{
            var favItem = {
                dish: item.dish,
                description: item.description,
                dishType: item.dishType,
                prepTime: item.prepTime,
                calories: item.calories,
                serving: item.serving,
                dishImage: item.dishImage,
                ingredients: item.ingredients,
                preparation: item.preparation,
            }
            dispatch(addDishToFavorites(favItem))
    
        }
       
    }

    const RecipeContainer = ({recipeName, recipesData}) => {
        var recipe_name = recipeName[0].toUpperCase() + recipeName.substring(1);


        return(
            <View style={{marginBottom: 20,}} >
                <View style={styles.recipeHeader} >
                    <Text style={styles.recipeType} >{recipe_name}</Text>
                    <TouchableOpacity
                    activeOpacity={0.5}
                        onPress={() => {}}
                    >
                        <Text style={styles.seeAll}>See All</Text>
                    </TouchableOpacity>
                </View>

                <View>
                    <FlatList
                        data={recipesData}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={item => item.dish} 
                        renderItem={({item}) => {
                  
                            return(
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    onPress={() => {navigation.navigate('RecipeDetails', {params: item})}}
                                    style={styles.recipeItemContainer} 
                                    key={item.dish}
                                >
                                    <View style={styles.recipeImageContainer}> 
                                       
                                        <Image
                                            source={{uri: item.dishImage}}
                                            style={{height: 200, width: 150, borderRadius: 8,}}
                                        />
                                        <View  style={styles.caloiesContainer} >
                                            <Image
                                                source={media.calories}
                                                style={{height: 18, width: 18, marginRight: 8}}
                                            />
                                            <Text>{item.calories} Cal</Text>
                                        </View>

                                        <TouchableOpacity 
                                            activeOpacity={0.5}
                                            onPress={() => {addDishToFav(item)}}
                                            style={styles.favoriteContainer} >
                                            <Image
                                                source={dishIncludedInFav(item.dish) ? media.heart_filled : media.heart_empty}
                                                style={{height: 18, width: 18,}}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    <Text numberOfLines={1} style={styles.dishName} >{item.dish}</Text>
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            </View>
        )
    }

    const RecipesList = () => {
        return(
            <View>
                {Object.keys(groupTodoByCategory(food_recipes_data)).map((recipe, index) => {
                    var recipesData = groupTodoByCategory(food_recipes_data)[recipe]

                    return(
                        <View key={recipe} >
                            <RecipeContainer recipeName={recipe} recipesData={recipesData} />
                        </View>
                    )
                })}
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <View style={styles.mainContainer} >
                    <RecipesList />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.light_orange1
    },
    mainContainer: {
        padding: 20,
    },
    recipeHeader: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    recipeType: {
        fontSize: fontSize.SubHeading,
        fontWeight: '600',
        color: colors.dark_blue
    },
    seeAll: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: '600',
        textDecorationLine: 'underline',
    },
    dishName: {
        fontSize: fontSize.SubTitle,
        color: colors.dark_blue,
        fontWeight: 'bold',
        marginTop: 10,
    },
    recipeItemContainer: {
        marginRight: 15, 
        width: 150,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 3,
        // },
        // shadowOpacity: 0.29,
        // shadowRadius: 4.65,

        // elevation: 7,
    },
    recipeImageContainer: {
        
    },
    caloiesContainer: {
        position: 'absolute',
        bottom: 0, 
        right: 0,
        height: 30,
        padding: 5,
        paddingHorizontal: 10,
        borderTopLeftRadius: 8,
        borderBottomRightRadius: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white,
    },
    favoriteContainer: {
        position: 'absolute',
        right: 15,
        top: 15,
        height: 36,
        width: 36,
        borderRadius: 18,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.dark_blue
    }
})

export default HomeScreen