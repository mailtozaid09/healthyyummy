import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';



import OnboardingStack from './onboarding';
import LoginStack from './login';
import HomeScreen from '../screens/home';
import HomeStack from './home';
import ProfileStack from './profile';
import Tabbar from './tabbar';

const Stack = createStackNavigator();


const Navigator = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="OnboardingStack" 
        >
            <Stack.Screen
                name="OnboardingStack"
                component={OnboardingStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={{
                    headerShown: false
                }}
            />


        </Stack.Navigator>
    );
}

export default Navigator